import { Component, OnInit } from '@angular/core';
import { DataService } from '../../services/data.service';
import { CartaObtenida } from '../../modules/cartaObtenida';

@Component({
  selector: 'app-album',
  templateUrl: './album.component.html',
  styleUrls: ['./album.component.css']
})
export class AlbumComponent implements OnInit {

  userLogin: string;
  miCartas: CartaObtenida[];
  temp: CartaObtenida[];
  imagen: string;
  equipo: string;
  equipos = [];
  carta: CartaObtenida;
  teamName: string;

  constructor(public dataService: DataService) {
    /*
    this.miCartas = [
      new CartaObtenida('assets/postales/19.jpg', false, 'a'),
      new CartaObtenida('assets/postales/20.jpg', false, 'a'),

      new CartaObtenida('assets/postales/22.jpg', false, 'a'),
      new CartaObtenida('assets/postales/23.jpg', false, 'a'),

      new CartaObtenida('assets/postales/25.jpg', false, 'a'),
      new CartaObtenida('assets/postales/26.jpg', false, 'a'),
      new CartaObtenida('assets/postales/27.jpg', false, 'a'),
      new CartaObtenida('assets/postales/28.jpg', false, 'a'),
      new CartaObtenida('assets/postales/29.jpg', false, 'a'),
      new CartaObtenida('assets/postales/30.jpg', false, 'a'),
      new CartaObtenida('assets/postales/31.jpg', false, 'a'),

      new CartaObtenida('assets/postales/33.jpg', false, 'a'),
      new CartaObtenida('assets/postales/34.jpg', false, 'a'),
      new CartaObtenida('assets/postales/35.jpg', false, 'a'),
      new CartaObtenida('assets/postales/36.jpg', false, 'a'),

      new CartaObtenida('assets/postales/38.jpg', false, 'a'),
      new CartaObtenida('assets/postales/39.jpg', false, 'a')
    ];
    */
  }
  //
  ngOnInit() {
    this.teamName = 'Rusia';
    this.userLogin = this.dataService.getLoginSesion();
    this.CargarTemp(20);
    this.miCartas = this.dataService.getCartaObtenida();
    let cartTemp;
    cartTemp = [];
    let count;
    count = 0;
    for (let index = 0; index < this.miCartas.length; index++) {
      let userCarta = this.miCartas[index].user;
      let repetida = this.miCartas[index].repetida;
      if (userCarta === this.userLogin && !repetida) {
        this.carta = new CartaObtenida(this.miCartas[index].carta, this.miCartas[index].repetida, this.miCartas[index].user);
        cartTemp.push(this.carta);
      }
    }
    this.miCartas = [];
    this.miCartas = cartTemp;
    let cargo;
    cargo = false;
    if (this.miCartas.length > 0) {
      for (let index = 0; index < this.temp.length; index++) {
        let valueTemp = this.temp[index].carta;
        valueTemp = valueTemp.replace('.jpg', '');
        for (let index2 = 0; index2 < this.miCartas.length; index2++) {
          let valueTemp2 = this.miCartas[index2].carta;
          valueTemp2 = valueTemp2.replace('.jpg', '');
          if (valueTemp === valueTemp2) {
            if (this.miCartas[index2].user === this.userLogin && this.miCartas[index2].repetida === false) {
              this.temp[index].carta = 'assets/postales/' + valueTemp + '.jpg';
              count = count + 1;
            }
            break;
          }
        }
      }
      for (let index = 0; index < this.temp.length; index++) {
        let carta = this.temp[index].carta;
        if (!carta.includes('.jpg')) {
          this.temp[index].carta = 'assets/postales/default.jpg';
        }
      }
    } else {
      for (let index = 0; index < this.temp.length; index++) {
        this.temp[index].carta = 'assets/postales/default.jpg';
      }
    }
    let porcentaje;
    porcentaje = 0;
    porcentaje = (count * 100) / 20;
    this.teamName = 'Rusia' + ' ' + count + '/20 (' + porcentaje + '%)';
    this.miCartas = this.temp;
  }
  //
  MisCartasTemp() {
    this.miCartas = [
      new CartaObtenida('19.jpg', false, 'a'),
      new CartaObtenida('20.jpg', false, 'a'),

      new CartaObtenida('22.jpg', false, 'a'),
      new CartaObtenida('23.jpg', false, 'a'),

      new CartaObtenida('25.jpg', false, 'a'),
      new CartaObtenida('26.jpg', false, 'a'),
      new CartaObtenida('27.jpg', false, 'a'),
      new CartaObtenida('28.jpg', false, 'a'),
      new CartaObtenida('29.jpg', false, 'a'),
      new CartaObtenida('30.jpg', false, 'a'),
      new CartaObtenida('31.jpg', false, 'a'),

      new CartaObtenida('33.jpg', false, 'a'),
      new CartaObtenida('34.jpg', false, 'a'),
      new CartaObtenida('35.jpg', false, 'a'),
      new CartaObtenida('36.jpg', false, 'a'),

      new CartaObtenida('38.jpg', false, 'a'),
      new CartaObtenida('39.jpg', false, 'a')
    ];
  }
  //
  onClick(event) {
    let target = event.target || event.srcElement || event.currentTarget;
    let idAttr = target.text;
    console.log(idAttr);
    let postalInicio;
    
    switch (idAttr) {
      case 'Rusia':
        postalInicio = 20;
        break;
      case 'Saudi Arabia':
        postalInicio = 40;
        break;
      case 'Egypt':
        postalInicio = 60;
        break;
      case 'Uruguay':
        postalInicio = 80;
        break;
      case 'Portugal':
        postalInicio = 100;
        break;
      case 'Spain':
        postalInicio = 120;
        break;
      case 'Morocco':
        postalInicio = 140;
        break;
      case 'Islamic Republic of Iran':
        postalInicio = 160;
        break;
      case 'France':
        postalInicio = 180;
        break;
      case 'Australia':
        postalInicio = 200;
        break;
      case 'Peru':
        postalInicio = 220;
        break;
      case 'Denmark':
        postalInicio = 240;
        break;
      case 'Argentina':
        postalInicio = 260;
        break;
      case 'Iceland':
        postalInicio = 280;
        break;
      case 'Croatia':
        postalInicio = 300;
        break;
      case 'Nigeria':
        postalInicio = 320;
        break;
      case 'Brazil':
        postalInicio = 340;
        break;
      case 'Switzerland':
        postalInicio = 360;
        break;
      case 'Costa Rica':
        postalInicio = 380;
        break;
      case 'Serbia':
        postalInicio = 400;
        break;
      case 'Germany':
        postalInicio = 420;
        break;
      case 'México':
        postalInicio = 440;
        break;
      case 'Sweden':
        postalInicio = 460;
        break;
      case 'Korea Republic':
        postalInicio = 480;
        break;
      case 'Belgium':
        postalInicio = 500;
        break;
      case 'Panamá':
        postalInicio = 520;
        break;
      case 'Tunisia':
        postalInicio = 540;
        break;
      case 'England':
        postalInicio = 560;
        break;
      case 'Poland':
        postalInicio = 580;
        break;
      case 'Senegal':
        postalInicio = 600;
        break;
      case 'Colombia':
        postalInicio = 620;
        break;
      case 'Japan':
        postalInicio = 640;
        break;
      default:
        break;
    }
    this.CargarTemp(postalInicio);
    this.miCartas = this.dataService.getCartaObtenida();
    // this.MisCartasTemp();
    let cartTemp;
    cartTemp = [];

    for (let index = 0; index < this.miCartas.length; index++) {
      let userCarta = this.miCartas[index].user;
      let repetida = this.miCartas[index].repetida;
      if (userCarta === this.userLogin && !repetida) {
        this.carta = new CartaObtenida(this.miCartas[index].carta, this.miCartas[index].repetida, this.miCartas[index].user);
        cartTemp.push(this.carta);
      }
    }
    this.miCartas = [];
    this.miCartas = cartTemp;
    let cargo;
    cargo = false;
    let count;
    count = 0;
    if (this.miCartas.length > 0) {
      for (let index = 0; index < this.temp.length; index++) {
        let valueTemp = this.temp[index].carta;
        valueTemp = valueTemp.replace('.jpg', '');
        for (let index2 = 0; index2 < this.miCartas.length; index2++) {
          let valueTemp2 = this.miCartas[index2].carta;
          valueTemp2 = valueTemp2.replace('.jpg', '');
          if (valueTemp === valueTemp2) {
            if (this.miCartas[index2].user === this.userLogin && this.miCartas[index2].repetida === false) {
              this.temp[index].carta = 'assets/postales/' + valueTemp + '.jpg';
              count = count + 1;
            }
            break;
          }
        }
      }
      for (let index = 0; index < this.temp.length; index++) {
        let carta = this.temp[index].carta;
        if (!carta.includes('.jpg')) {
          this.temp[index].carta = 'assets/postales/default.jpg';
        }
      }
    } else {
      for (let index = 0; index < this.temp.length; index++) {
        this.temp[index].carta = 'assets/postales/default.jpg';
      }
    }
    let porcentaje;
    porcentaje = 0;
    porcentaje = (count * 100) / 20;
    this.teamName = idAttr + ' ' + count + '/20 (' + porcentaje + '%)';
    this.miCartas = this.temp;
  }
  //
  CargarTemp(postalInicio: number) {

    this.temp = [
      new CartaObtenida((postalInicio).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 1).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 2).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 3).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 4).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 5).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 6).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 7).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 8).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 9).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 10).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 11).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 12).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 13).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 14).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 15).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 16).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 17).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 18).toString(), false, this.userLogin),
      new CartaObtenida((postalInicio + 19).toString(), false, this.userLogin)
    ];
  }
}
