import { Component, OnInit } from '@angular/core';
<<<<<<< HEAD
=======
import { DataService } from '../services/data.service';

import { CartaObtenida } from '../modules/cartaObtenida';
import { Equipo } from '../modules/equipo';
>>>>>>> 8e72d8f93d18f5c498a7ee05a72da60e64e69501

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
<<<<<<< HEAD
export class PrincipalComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

=======

export class PrincipalComponent implements OnInit {

  userLogin: string;
  cartasObtenida: CartaObtenida[];
  cantCartasTotal: number;
  cantCartasRepetidas: number;
  cantCartasUnicas: number;
  albumCompleto: number;
  carta: number;
  newCartObtenida: CartaObtenida;
  equipoCompleto: number;
  team: Equipo;
  equipos: Equipo[];
  temp: CartaObtenida[];

  constructor(public dataService: DataService) {
    this.limpiarEstadistica();
    this.crearEquipos();
    this.userLogin = this.dataService.getLoginSesion();

  }
  //
  ngOnInit() {
    /*Obtiene la sesion que esta iniciada*/
    this.userLogin = this.dataService.getLoginSesion();
    this.cargarEstadistica();
  }
  
  /*Arreglo de los equipos del album*/
  crearEquipos() {
    this.equipos = [
      new Equipo(1, 0),
      new Equipo(2, 0),
      new Equipo(3, 0),
      new Equipo(4, 0),
      new Equipo(5, 0),
      new Equipo(6, 0),
      new Equipo(7, 0),
      new Equipo(8, 0),
      new Equipo(9, 0),
      new Equipo(10, 0),
      new Equipo(11, 0),
      new Equipo(12, 0),
      new Equipo(13, 0),
      new Equipo(14, 0),
      new Equipo(15, 0),
      new Equipo(16, 0),
      new Equipo(17, 0),
      new Equipo(18, 0),
      new Equipo(19, 0),
      new Equipo(20, 0),
      new Equipo(21, 0),
      new Equipo(22, 0),
      new Equipo(23, 0),
      new Equipo(24, 0),
      new Equipo(25, 0),
      new Equipo(26, 0),
      new Equipo(27, 0),
      new Equipo(28, 0),
      new Equipo(29, 0),
      new Equipo(30, 0),
      new Equipo(31, 0),
      new Equipo(32, 0)
    ];
  }
  
  /*Limpia las estadisticas*/
  limpiarEstadistica() {
    this.cantCartasTotal = 0;
    this.cantCartasRepetidas = 0;
    this.cantCartasUnicas = 0;
    this.albumCompleto = 0;
    this.equipoCompleto = 0;
  }
  
  /*Arreglo de cartas temporales*/
  MisCartasTemp() {
    this.temp = [
      new CartaObtenida('20.jpg', false, 'elsie'),
      new CartaObtenida('21.jpg', false, 'elsie'),
      new CartaObtenida('22.jpg', false, 'elsie'),
      new CartaObtenida('23.jpg', false, 'elsie'),
      new CartaObtenida('24.jpg', false, 'elsie'),
      new CartaObtenida('25.jpg', false, 'elsie'),
      new CartaObtenida('26.jpg', false, 'elsie'),
      new CartaObtenida('27.jpg', false, 'elsie'),
      new CartaObtenida('28.jpg', false, 'elsie'),
      new CartaObtenida('29.jpg', false, 'elsie'),
      new CartaObtenida('30.jpg', false, 'elsie'),
      new CartaObtenida('31.jpg', false, 'elsie'),
      new CartaObtenida('32.jpg', false, 'elsie'),
      new CartaObtenida('33.jpg', false, 'elsie'),
      new CartaObtenida('34.jpg', false, 'elsie'),
      new CartaObtenida('35.jpg', false, 'elsie'),
      new CartaObtenida('36.jpg', false, 'elsie'),
      new CartaObtenida('37.jpg', false, 'elsie'),
      new CartaObtenida('38.jpg', false, 'elsie'),
      new CartaObtenida('39.jpg', false, 'elsie'),
      new CartaObtenida('40.jpg', false, 'elsie'),
      new CartaObtenida('41.jpg', false, 'elsie'),
      new CartaObtenida('42.jpg', false, 'elsie'),
      new CartaObtenida('43.jpg', false, 'elsie'),
      new CartaObtenida('44.jpg', false, 'elsie'),
      new CartaObtenida('45.jpg', false, 'elsie'),
      new CartaObtenida('46.jpg', false, 'elsie'),
      new CartaObtenida('47.jpg', false, 'elsie'),
      new CartaObtenida('48.jpg', false, 'elsie'),
      new CartaObtenida('49.jpg', false, 'elsie'),
      new CartaObtenida('50.jpg', false, 'elsie'),
      new CartaObtenida('51.jpg', false, 'elsie'),
      new CartaObtenida('52.jpg', false, 'elsie'),
      new CartaObtenida('53.jpg', false, 'elsie'),
      new CartaObtenida('54.jpg', false, 'elsie'),
      new CartaObtenida('55.jpg', false, 'elsie'),
      new CartaObtenida('56.jpg', false, 'elsie'),
      new CartaObtenida('57.jpg', false, 'elsie'),
      new CartaObtenida('58.jpg', false, 'elsie'),
      new CartaObtenida('59.jpg', false, 'elsie'),
      new CartaObtenida('39.jpg', true, 'elsie'),
      new CartaObtenida('31.jpg', true, 'elsie')
    ];
  }

/*Genera las estadisticas del album*/
  cargarEstadistica() {
    this.limpiarEstadistica();
    this.crearEquipos();
    this.cartasObtenida = this.dataService.getCartaObtenida();
    // this.MisCartasTemp();
    // this.cartasObtenida = this.temp;

    for (let index = 0; index < this.cartasObtenida.length; index++) {
      if (this.cartasObtenida[index].user === this.userLogin) {
        if (this.cartasObtenida.length >= 1 ) {
          /*Cantidad de cartas totales*/
          this.cantCartasTotal += 1;
          if (this.cartasObtenida[index].repetida) {
            /*Cantidad de cartas repetidas*/
            this.cantCartasRepetidas += 1;
          } else {
            /*Cantidad de cartas sin repetir*/
            this.cantCartasUnicas += 1;
          }
        }
      }
    }
    /*Obtiene el % completado del album*/
    this.albumCompleto = (this.cantCartasUnicas * 100) / 640;

    this.obtenerEquiposCompletos();
  }
  
  /*Genera la estadistica que los equipos completos*/
  obtenerEquiposCompletos() {
    for (let index = 0; index < this.cartasObtenida.length; index++) {
      if (this.cartasObtenida[index].user === this.userLogin && this.cartasObtenida[index].repetida === false)  {
        let valueTemp = this.cartasObtenida[index].carta;
        // tslint:disable-next-line:radix
        let postal = parseInt(valueTemp.replace('.jpg', ''));
        if (postal >= 20 && postal <= 39) {
          this.equipos[0].cant_cartas = this.equipos[0].cant_cartas + 1;
        } else if (postal >= 40 && postal <= 59) {
          this.equipos[1].cant_cartas = this.equipos[1].cant_cartas + 1;
        } else if (postal >= 60 && postal <= 79) {
          this.equipos[2].cant_cartas = this.equipos[2].cant_cartas + 1;
        } else if (postal >= 80 && postal <= 99) {
          this.equipos[3].cant_cartas = this.equipos[3].cant_cartas + 1;
        } else if (postal >= 100 && postal <= 119) {
          this.equipos[4].cant_cartas = this.equipos[4].cant_cartas + 1;
        } else if (postal >= 120 && postal <= 139) {
          this.equipos[5].cant_cartas = this.equipos[5].cant_cartas + 1;
        } else if (postal >= 140 && postal <= 159) {
          this.equipos[6].cant_cartas = this.equipos[6].cant_cartas + 1;
        } else if (postal >= 160 && postal <= 179) {
          this.equipos[7].cant_cartas = this.equipos[7].cant_cartas + 1;
        } else if (postal >= 180 && postal <= 199) {
          this.equipos[8].cant_cartas = this.equipos[8].cant_cartas + 1;
        } else if (postal >= 200 && postal <= 219) {
          this.equipos[9].cant_cartas = this.equipos[9].cant_cartas + 1;
        } else if (postal >= 220 && postal <= 239) {
          this.equipos[10].cant_cartas = this.equipos[10].cant_cartas + 1;
        } else if (postal >= 240 && postal <= 259) {
          this.equipos[11].cant_cartas = this.equipos[11].cant_cartas + 1;
        } else if (postal >= 260 && postal <= 279) {
          this.equipos[12].cant_cartas = this.equipos[12].cant_cartas + 1;
        } else if (postal >= 280 && postal <= 299) {
          this.equipos[13].cant_cartas = this.equipos[13].cant_cartas + 1;
        } else if (postal >= 300 && postal <= 319) {
          this.equipos[14].cant_cartas = this.equipos[14].cant_cartas + 1;
        } else if (postal >= 320 && postal <= 339) {
          this.equipos[15].cant_cartas = this.equipos[15].cant_cartas + 1;
        } else if (postal >= 340 && postal <= 359) {
          this.equipos[16].cant_cartas = this.equipos[16].cant_cartas + 1;
        } else if (postal >= 360 && postal <= 379) {
          this.equipos[17].cant_cartas = this.equipos[17].cant_cartas + 1;
        } else if (postal >= 380 && postal <= 399) {
          this.equipos[18].cant_cartas = this.equipos[18].cant_cartas + 1;
        } else if (postal >= 400 && postal <= 419) {
          this.equipos[19].cant_cartas = this.equipos[19].cant_cartas + 1;
        } else if (postal >= 420 && postal <= 439) {
          this.equipos[20].cant_cartas = this.equipos[20].cant_cartas + 1;
        } else if (postal >= 440 && postal <= 459) {
          this.equipos[21].cant_cartas = this.equipos[21].cant_cartas + 1;
        } else if (postal >= 460 && postal <= 479) {
          this.equipos[22].cant_cartas = this.equipos[22].cant_cartas + 1;
        } else if (postal >= 480 && postal <= 499) {
          this.equipos[23].cant_cartas = this.equipos[23].cant_cartas + 1;
        } else if (postal >= 500 && postal <= 519) {
          this.equipos[24].cant_cartas = this.equipos[24].cant_cartas + 1;
        } else if (postal >= 520 && postal <= 539) {
          this.equipos[25].cant_cartas = this.equipos[25].cant_cartas + 1;
        } else if (postal >= 540 && postal <= 559) {
          this.equipos[26].cant_cartas = this.equipos[26].cant_cartas + 1;
        } else if (postal >= 560 && postal <= 579) {
          this.equipos[27].cant_cartas = this.equipos[27].cant_cartas + 1;
        } else if (postal >= 580 && postal <= 599) {
          this.equipos[28].cant_cartas = this.equipos[28].cant_cartas + 1;
        } else if (postal >= 600 && postal <= 619) {
          this.equipos[29].cant_cartas = this.equipos[29].cant_cartas + 1;
        } else if (postal >= 620 && postal <= 639) {
          this.equipos[30].cant_cartas = this.equipos[30].cant_cartas + 1;
        } else if (postal >= 640 && postal <= 659) {
          this.equipos[31].cant_cartas = this.equipos[31].cant_cartas + 1;
        } else if (postal >= 660 && postal <= 679) {
          this.equipos[32].cant_cartas = this.equipos[32].cant_cartas + 1;
        }
      }
    }
    /*Recorrre el arreglo de equipos para ver si estan completos*/
    for (let index = 0; index < this.equipos.length; index++) {
      if (this.equipos[index].cant_cartas === 20) {
        this.equipoCompleto += 1;
      }
    }
  }
  
  /*Elimina y cierra la sesion que esta habierta*/
  cerrar() {
    this.dataService.deleteSesion();
  }
  
  /*Genera las cartas nuevas de cada sobre y las guarda*/
  obtenerCarta() {
    for (let index = 1; index <= 5; index++) {
      this.carta = Math.floor((Math.random() * 640) + 20);
      this.cartasObtenida = this.dataService.getCartaObtenida();
      if (this.cartasObtenida.length === 0) {
        // tslint:disable-next-line:max-line-length
        this.newCartObtenida = new CartaObtenida(this.carta + '.jpg', this.dataService.validarRepetida(this.carta + '.jpg'), this.userLogin);
        /*Envia la carta nueva pero repetida*/
        this.dataService.newCartaObtenida = this.newCartObtenida;
        /*Agrega la nueva postal al arreglo*/
        this.dataService.addCartaObtenida();
      } else {
        // tslint:disable-next-line:max-line-length
        this.newCartObtenida = new CartaObtenida(this.carta + '.jpg', this.dataService.validarRepetida(this.carta + '.jpg'), this.userLogin);
        /*Envia la carta nueva sin repetir*/
        this.dataService.newCartaObtenida = this.newCartObtenida;
        /*Agrega la nueva postal al arreglo*/
        this.dataService.addCartaObtenida();
      }
    }
    this.cargarEstadistica();
  }
>>>>>>> 8e72d8f93d18f5c498a7ee05a72da60e64e69501
}
