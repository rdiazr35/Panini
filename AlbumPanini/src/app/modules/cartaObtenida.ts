export class CartaObtenida {
    carta: string;
    repetida: boolean;
    user: string;

    constructor(carta: string, repetida: boolean, user:string) {
        this.carta = carta;
        this.repetida = repetida;
        this.user = user;
    }
}
