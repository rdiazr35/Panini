export class Equipo {
    equipo: number;
    cant_cartas: number;

    constructor(equipo: number, cant_cartas: number) {
        this.equipo = equipo;
        this.cant_cartas = cant_cartas;
    }
}
